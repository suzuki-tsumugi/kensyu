package jp.co.kensyu;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kensyu.dto.test.TestDto;
import jp.co.kensyu.service.TestService;

@Controller
public class TestController1 {

	@Autowired
	//DIを使ってnew(インスタンス化）することなく
	// インターフェースを使って実クラスをインスタンス化する
	private TestService testService;

	@RequestMapping(value = "/test/{id}", method = RequestMethod.GET)
	public String test(Model model, @PathVariable int id) {
		TestDto test = testService.getTest(id);
		//Service経由で取得したDBセレクト情報をTestDtoという入れ物に詰めてあげて
		model.addAttribute("message", "MyBatisのサンプルです");
		model.addAttribute("test", test);
		//結果を"test"というkey値でmodelにAttributeしてる
		return "test";
	}
	@RequestMapping(value = "/test/", method = RequestMethod.GET)
	public String testAll(Model model) {
	    List<TestDto> tests = testService.getTestAll();
	    model.addAttribute("message", "MyBatisの全件取得サンプルです");
	    model.addAttribute("tests", tests);
	    return "tests";
	}
	@RequestMapping(value = "/test/dto/{id}", method = RequestMethod.GET)
	public String testDto(Model model, @PathVariable int id) {
	    TestDto dto = new TestDto();
	    dto.setId(id);
	    TestDto test = testService.getTestByDto(dto);
	    model.addAttribute("message", "MyBatisのサンプルです");
	    model.addAttribute("test", test);
	    return "test";
	}
}