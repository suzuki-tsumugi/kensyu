package jp.co.kensyu.entity;
//Mapperを通して、select結果をTestentityに詰めて返却する
public class Test {
	private Integer id;
    private String name;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
