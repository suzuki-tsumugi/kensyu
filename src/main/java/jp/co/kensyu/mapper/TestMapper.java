package jp.co.kensyu.mapper;

import java.util.List;

import jp.co.kensyu.dto.test.TestDto;
import jp.co.kensyu.entity.Test;

/*TestMapper.javaはinterfaceです。

mvc-configに以下の記述があるおかげで、

<!-- どこのパッケージをmapperとして認識するかの設定 -->
<bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
    <property name="basePackage" value="jp.co.kenshu.mapper" />
</bean>
mapperパッケージ下をMyBatisのORM機能が使えるDaoとして認識*/

public interface TestMapper {
    Test getTest(int id);
    //TestMapprer.javaは、メソッドがコールされた際に、同じパッケージにあるTestMapper.xmlを利用
//getTestはxmlのid="getTest"と紐いて処理が行われる仕組み
    List<Test> getTestAll();
    Test getTestByDto(TestDto dto);
}
