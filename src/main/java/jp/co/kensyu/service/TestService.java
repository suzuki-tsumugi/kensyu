package jp.co.kensyu.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.kensyu.dto.test.TestDto;
import jp.co.kensyu.entity.Test;
import jp.co.kensyu.mapper.TestMapper;

@Service
public class TestService {

    @Autowired
    private TestMapper testMapper;//インターフェースを使って実クラスをインスタンス化

    public TestDto getTest(Integer id) {
        TestDto dto = new TestDto();
        Test entity = testMapper.getTest(id);
        //Mapper(Dao)経由で取得したentityを
        
        BeanUtils.copyProperties(entity, dto);//←を使ってTestDtoに詰め替えてreturnしている
        return dto;
    }
    
    public TestDto getTestByDto(TestDto dto) {
        Test entity = testMapper.getTestByDto(dto);
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }
    
    public List<TestDto> getTestAll() {
    	//getTestAllでmapperから取得した結果をListに詰める。この段階ではTestはentity。
        List<Test> testList = testMapper.getTestAll();
        //convertToDto()で、リストに詰められたentityをDtoに詰めなおす
        List<TestDto> resultList = convertToDto(testList);
        return resultList;
    }

    private List<TestDto> convertToDto(List<Test> testList) {
    	
        List<TestDto> resultList = new LinkedList<TestDto>();
        for (Test entity : testList) {
            TestDto dto = new TestDto();
            BeanUtils.copyProperties(entity, dto);
            resultList.add(dto);
        }
        return resultList;
    }
   
}