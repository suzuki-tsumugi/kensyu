<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta charset="utf-8">
        <title>Welcome</title>
    </head>
    <body>

           <h1>${message}</h1>
           <!--Controllerから渡されたtestsをまわす。
           var = testなのでitemsから取り出された要素は変数testへ格納
           ｛test｝がTestDtoになる
           結果的に、${test.name}としているため、
           画面上には「test.getName()」した結果が表示される -->
        <c:forEach items="${tests}" var="test">
            <p><c:out value="${test.name}"></c:out></p>
        </c:forEach>
    </body>
</html>